package com.example.demo.service.Implementation;

import com.example.demo.repository.CategoryRepos;
import com.example.demo.repository.dto.CategoryDto;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImplement implements CategoryService {

    private CategoryRepos categoryRepos;
    @Autowired
    public CategoryServiceImplement(CategoryRepos categoryRepos) {
        this.categoryRepos = categoryRepos;
    }

    @Override
    public CategoryDto insert(CategoryDto categoryDto) {
        boolean inInserted = categoryRepos.insert(categoryDto);
        if (inInserted)
            return categoryDto;
        else
            return null;
    }

    @Override
    public List<CategoryDto> select() {
        return categoryRepos.select();
    }

    @Override
    public boolean delete(int id) {
        boolean status = categoryRepos.delete(id);
        if (status){
            return status;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean update(CategoryDto categoryDto, int id) {
        boolean status = categoryRepos.update(categoryDto,id);
        if (status){
            return status;
        }
        else {
            return false;
        }
    }
}
