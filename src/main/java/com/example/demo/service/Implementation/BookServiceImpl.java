package com.example.demo.service.Implementation;

import com.example.demo.repository.dto.BookDto;
import com.example.demo.repository.BookRepos;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private BookRepos bookRepos;
    @Autowired
    public BookServiceImpl(BookRepos bookRepos) {
        this.bookRepos = bookRepos;
    }

    @Override
    public BookDto insert(BookDto bookDto) {
        boolean inInserted = bookRepos.insert(bookDto);
        if (inInserted)
            return bookDto;
        else
            return null;
    }

    @Override
    public List<BookDto> select() {
        return bookRepos.select();
    }

    @Override
    public boolean delete(int id) {
        boolean status = bookRepos.delete(id);
        if (status){
            return status;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean update(BookDto bookDto, int id) {
        boolean status = bookRepos.update(bookDto,id);
        if (status){
            return status;
        }
        else {
            return false;
        }
    }
}
