package com.example.demo.service.Implementation;

import com.example.demo.repository.UserRepos;
import com.example.demo.repository.dto.RoleDto;
import com.example.demo.repository.dto.UserDto;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserServiceImp implements UserService {

    UserRepos userRepos;
    @Autowired
    public UserServiceImp(UserRepos userRepos) {
        this.userRepos = userRepos;
    }

    //Log In
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepos.loadUserByUsername(username);
    }

    @Override
    public UserDto insert(UserDto userDto) {

        try {
            boolean isInserted = userRepos.insert(userDto);

            if (isInserted) {

                //Take id from table user and insert to table users_roles
                userDto.setId(userRepos.selectIdByUserId(userDto.getUserId()));

                //Add data to table users_roles
                for (RoleDto roleDto : userDto.getRoles()){
                    userRepos.createUserRoles(userDto, roleDto);
                }

            }
            return isInserted ? userDto : null;
        }
        catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
//                    Messsages.Error.INSERTED_FAILURE.getMessage()
                    e.getMessage()
            );
        }
    }

    @Override
    public boolean createUserRoles(UserDto user, RoleDto roleDto){
        return userRepos.createUserRoles(user, roleDto);
    }

}
