package com.example.demo.service;

import com.example.demo.repository.dto.RoleDto;
import com.example.demo.repository.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    UserDto insert(UserDto userDto);
    boolean createUserRoles(UserDto user, RoleDto role);

}
