package com.example.demo.service;

import com.example.demo.repository.dto.BookDto;

import java.util.List;

public interface BookService {

    BookDto insert(BookDto booksDto);
    List<BookDto> select();
    boolean delete(int id);
    boolean update(BookDto bookDto, int id);

}