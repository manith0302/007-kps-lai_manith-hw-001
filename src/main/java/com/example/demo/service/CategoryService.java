package com.example.demo.service;

import com.example.demo.repository.dto.CategoryDto;

import java.util.List;

public interface CategoryService {

    CategoryDto insert(CategoryDto categoryDto);
    List<CategoryDto> select();
    boolean delete(int id);
    boolean update(CategoryDto categoryDto, int id);

}
