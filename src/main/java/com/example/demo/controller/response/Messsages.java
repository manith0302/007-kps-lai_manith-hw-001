package com.example.demo.controller.response;

public class Messsages {

    public enum Success{
        INSERTED_SUCCESS("Record have added successful");

        private String message;

        Success(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public enum Error {

        INSERTED_FAILURE("Record is been unable to insert.");

        private String message;

        Error(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

}
