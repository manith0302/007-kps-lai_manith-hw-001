package com.example.demo.controller.request;

import com.example.demo.repository.dto.RoleRequestModel;

import java.util.ArrayList;

public class UserRequestModel {

    private String username;
    private String password;
    private String confirmPassword;

    ArrayList<RoleRequestModel> roles;

    public UserRequestModel() {
    }

    public UserRequestModel(String username, String password, String confirmPassword, ArrayList<RoleRequestModel> roles) {
        this.username = username;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public ArrayList<RoleRequestModel> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<RoleRequestModel> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "UserRequestModel{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", roles=" + roles +
                '}';
    }
}
