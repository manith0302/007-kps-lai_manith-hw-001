package com.example.demo.controller.restcontroller;

import com.example.demo.controller.request.UserRequestModel;
import com.example.demo.controller.response.BaseApiResponse;
import com.example.demo.controller.response.Messsages;
import com.example.demo.controller.response.UserResponse;
import com.example.demo.controller.restcontroller.utils.ApiUtil;
import com.example.demo.controller.restcontroller.utils.DateTimeUtil;
import com.example.demo.repository.dto.UserDto;
import com.example.demo.service.Implementation.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.UUID;

@RestController
public class UserController {

    UserServiceImp userServiceImp;
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserServiceImp(UserServiceImp userServiceImp) {
        this.userServiceImp = userServiceImp;
    }

    @PostMapping("/users")
    public ResponseEntity<BaseApiResponse<UserResponse>> insert (@RequestBody UserRequestModel user) {
        BaseApiResponse<UserResponse> response = new BaseApiResponse<>();

        UserDto userDto = ApiUtil.mapper().map(user, UserDto.class);

        UUID uuid = UUID.randomUUID();
        String randomId = String.valueOf(uuid);
        userDto.setUserId(randomId);

        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));

        UserDto result = userServiceImp.insert(userDto);

        UserResponse result1 = ApiUtil.mapper().map(result, UserResponse.class);

        response.setSuccess(true);
        response.setMessage(Messsages.Success.INSERTED_SUCCESS.getMessage());
        response.setData(result1);
        response.setTime(DateTimeUtil.timestamp());
        response.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(response);

    }
}
