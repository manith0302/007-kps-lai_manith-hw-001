package com.example.demo.controller.restcontroller;

import com.example.demo.controller.request.CategoryRequestModel;
import com.example.demo.controller.response.BaseApiResponse;
import com.example.demo.repository.dto.CategoryDto;
import com.example.demo.service.Implementation.CategoryServiceImplement;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CategoryRestController {

    private CategoryServiceImplement categoryServiceImp;
    @Autowired
    public CategoryRestController(CategoryServiceImplement categoryServiceImp) {
        this.categoryServiceImp = categoryServiceImp;
    }

    @PostMapping("/categories")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert (
            @RequestBody CategoryRequestModel category ) {

        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        CategoryDto categoryDto = mapper.map(category, CategoryDto.class);

        CategoryDto result = categoryServiceImp.insert(categoryDto);

        CategoryRequestModel result2 = mapper.map(result, CategoryRequestModel.class);

        response.setMessage("Record have added successful.");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @GetMapping("/categories")
    public ResponseEntity<BaseApiResponse<List<CategoryRequestModel>>> select() {

        BaseApiResponse<List<CategoryRequestModel>> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        List<CategoryDto> categoryDtoList = categoryServiceImp.select();
        List<CategoryRequestModel> categoryRequestModels = new ArrayList<>();
        for (CategoryDto read : categoryDtoList) {
            categoryRequestModels.add(mapper.map(read, CategoryRequestModel.class));
        }
        response.setMessage("Books are found completely.");
        response.setStatus(HttpStatus.OK);
        response.setData(categoryRequestModels);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/categories/{id}")
    public boolean delete(@PathVariable int id) {
        return categoryServiceImp.delete(id);
    }

    @PutMapping("/categories/{id}")
    public boolean update(CategoryDto categoryDto, @PathVariable int id) {

        CategoryDto newCategory = new CategoryDto();
        newCategory.setTitle(categoryDto.getTitle());

        System.out.println(newCategory);

        return true;
    }

}
