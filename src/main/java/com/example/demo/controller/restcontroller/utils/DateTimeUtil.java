package com.example.demo.controller.restcontroller.utils;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class DateTimeUtil {
    public static Timestamp timestamp(){
        return new Timestamp(System.currentTimeMillis());
    }
}
