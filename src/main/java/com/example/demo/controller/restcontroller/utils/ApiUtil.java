package com.example.demo.controller.restcontroller.utils;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ApiUtil {

    public static ModelMapper mapper() {
        return new ModelMapper();
    }

}
