package com.example.demo.controller.restcontroller;

import com.example.demo.repository.dto.BookDto;
import com.example.demo.controller.request.BookRequestModel;
import com.example.demo.controller.response.BaseApiResponse;
import com.example.demo.service.Implementation.BookServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class BookRestController {

    private BookServiceImpl bookService;

    @Autowired
    public void setArticleService(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/books")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert (
            @RequestBody BookRequestModel book ) {

        BaseApiResponse<BookRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        BookDto bookDto = mapper.map(book, BookDto.class);

        bookDto.setCategoryId(2);

        BookDto result = bookService.insert(bookDto);

        BookRequestModel result2 = mapper.map(result, BookRequestModel.class);

        response.setMessage("Record have added successful.");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @GetMapping("/books")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> select() {

        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        List<BookDto> bookDtoList = bookService.select();
        List<BookRequestModel> bookRequestModels = new ArrayList<>();
        for (BookDto read : bookDtoList) {
            bookRequestModels.add(mapper.map(read, BookRequestModel.class));
        }
        response.setMessage("Books are found completely.");
        response.setStatus(HttpStatus.OK);
        response.setData(bookRequestModels);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/books/{id}")
    public boolean delete(@PathVariable int id) {
        return bookService.delete(id);
    }

    @PutMapping("/books/{id}")
    public boolean update(BookDto bookDto,@PathVariable int id) {

        BookDto newBook = new BookDto();
        newBook.setTitle(bookDto.getTitle());
        newBook.setAuthor(bookDto.getAuthor());
        newBook.setDescription(bookDto.getDescription());
        newBook.setThumbnail(bookDto.getThumbnail());
        newBook.setCategoryId(bookDto.getCategoryId());

        System.out.println(newBook);

        return true;
    }
}
