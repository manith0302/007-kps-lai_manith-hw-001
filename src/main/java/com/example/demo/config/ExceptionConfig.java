package com.example.demo.config;

import com.example.demo.controller.response.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

@ControllerAdvice
public class ExceptionConfig {

    @ExceptionHandler({ResponseStatusException.class})
    public ResponseEntity<ErrorResponse> exception (ResponseStatusException ex) {

        ErrorResponse response = new ErrorResponse();
        response.setCode(9999);
        response.setDetails(ex.getMessage());
        return ResponseEntity.ok(response);

    }
}
