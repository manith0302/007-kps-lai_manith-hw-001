package com.example.demo.repository;

import com.example.demo.repository.dto.RoleDto;
import com.example.demo.repository.dto.UserDto;
import com.example.demo.repository.provider.RoleProvider;
import com.example.demo.repository.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepos {

    @InsertProvider(type = UserProvider.class, method = "insertUser")
    boolean insert(UserDto userDto) throws Exception;

    @InsertProvider(type = RoleProvider.class, method = "createRole")
    boolean createUserRoles(UserDto user, RoleDto role);

    @SelectProvider(type = RoleProvider.class, method = "selectUserByIdSql")
    int selectIdByUserId(String userId);

    @SelectProvider(type = UserProvider.class, method = "loadUserByUsername")
    @Results({
            @Result(column = "user_id", property = "userId"),
            @Result(column = "id",property = "roles", many = @Many(select = "selectRolesByUserId"))
    })
    UserDto loadUserByUsername(String username);

    @Select("select r.id, r.name from roles r \n" +
            "inner join users_roles ur on r.id = ur.role_id\n" +
            "where ur.user_id = #{id}")
    List<RoleDto> selectRolesByUserId (int id);

}
