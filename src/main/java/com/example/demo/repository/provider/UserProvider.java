package com.example.demo.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

    //Insert data to table users
    public String insertUser () {
        return new SQL() {
            {
                INSERT_INTO("users");
                VALUES("user_id", "#{userId}");
                VALUES("username", "#{username}");
                VALUES("password", "#{password}");
            }
        }.toString();
    }

    //Log in by username of table users
    public String loadUserByUsername (String username) {
        return new SQL() {
            {
                SELECT("*");
                FROM("users");
                WHERE("username = #{username}");
            }
        }.toString();
    }

    public String createRole () {
        return new SQL() {
            {
                INSERT_INTO("users_roles");
                VALUES("user_id", "#{user.id}");
                VALUES("role_id", "#{role.id}");
            }
        }.toString();
    }

    public String selectUserByIdSql(String userId) {
        return new SQL() {
            {
                SELECT("id");
                FROM("users");
                WHERE("user_id = '" + userId + "'");
            }
        }.toString();
    }

}
