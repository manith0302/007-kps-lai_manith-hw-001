package com.example.demo.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    //Insert data to table books
    public String insertCategories () {
        return new SQL() {
            {
                INSERT_INTO("tb_categories");
                VALUES("title", "#{title}");
            }
        }.toString();
    }

    public String selectCategories() {
        return new SQL(){
            {
                SELECT("*");
                FROM("tb_categories");
            }
        }.toString();
    }

    public String deleteCategory (int id){
        return new SQL() {
            {
                DELETE_FROM("tb_categories");
                WHERE("id ='" + id + "'");
            }
        }.toString();
    }

}
