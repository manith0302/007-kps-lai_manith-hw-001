package com.example.demo.repository.provider;

import com.example.demo.repository.dto.BookDto;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    //Insert data to table books
    public String insertBooks () {
        return new SQL() {
            {
                INSERT_INTO("tb_books");
                VALUES("title", "#{title}");
                VALUES("author", "#{author}");
                VALUES("description", "#{description}");
                VALUES("thumbnail", "#{thumbnail}");
                VALUES("category_id", "#{categoryId}");
            }
        }.toString();
    }

    public String selectBooks() {
        return new SQL(){
            {
                SELECT("*");
                FROM("tb_books");
            }
        }.toString();
    }

    public String deleteBook (int id){
        return new SQL() {
            {
                DELETE_FROM("tb_books");
                WHERE("id ='" + id + "'");
            }
        }.toString();
    }
}
