package com.example.demo.repository.provider;
import org.apache.ibatis.jdbc.SQL;

public class RoleProvider {

    public String createRole () {
        return new SQL() {
            {
                INSERT_INTO("users_roles");
                VALUES("user_id", "#{user.id}");
                VALUES("role_id", "#{role.id}");
            }
        }.toString();
    }

    public String selectUserByIdSql(String userId) {
        return new SQL() {
            {
                SELECT("id");
                FROM("users");
                WHERE("user_id = '" + userId + "'");
            }
        }.toString();
    }

}
