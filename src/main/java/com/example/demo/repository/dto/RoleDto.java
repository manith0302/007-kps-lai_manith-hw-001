package com.example.demo.repository.dto;

import org.springframework.security.core.GrantedAuthority;

public class RoleDto implements GrantedAuthority {

    private int id;
    private String name;

    public RoleDto() {
    }

    public RoleDto(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoleDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    //return role for @override method type GrantedAuthority
    @Override
    public String getAuthority() {
        return name;
    }
}
