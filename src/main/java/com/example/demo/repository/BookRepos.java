package com.example.demo.repository;

import com.example.demo.repository.dto.BookDto;
import com.example.demo.repository.dto.CategoryDto;
import com.example.demo.repository.provider.BookProvider;
import com.example.demo.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepos {

    @InsertProvider(type = BookProvider.class, method = "insertBooks")
    boolean insert(BookDto bookDto);

    @SelectProvider(value = BookProvider.class, method = "selectBooks")
    List<BookDto> select();

    //    @Delete("delete from tb_books where id = #{id}")
    @DeleteProvider(type = BookProvider.class, method = "deleteBook")
    boolean delete(int id);

    @Update("UPDATE tb_books SET "
            + "title = #{book.title},"
            + "author = #{book.author},"
            + "description = #{book.description},"
            + "thumbnail = #{book.thumbnail}"
            + "WHERE id = #{id}")
    boolean update(@Param("book") BookDto bookDto, int id);
}
