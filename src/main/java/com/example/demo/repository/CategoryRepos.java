package com.example.demo.repository;

import com.example.demo.repository.dto.BookDto;
import com.example.demo.repository.dto.CategoryDto;
import com.example.demo.repository.provider.BookProvider;
import com.example.demo.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepos {

    @InsertProvider(type = CategoryProvider.class, method = "insertCategories")
    boolean insert(CategoryDto categoryDto);

    @SelectProvider(value = CategoryProvider.class, method = "selectCategories")
    List<CategoryDto> select();

    @DeleteProvider(type = CategoryProvider.class, method = "deleteCategory")
    boolean delete(int id);

    @Update("UPDATE tb_categories SET "
            + "title = #{category.title},")
    boolean update(@Param("category") CategoryDto categoryDto, int id);

}
